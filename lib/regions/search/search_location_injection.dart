import 'package:core_modules/regions/region_injection.dart';
import 'package:core_modules/regions/search/domain/usecases/get_location_by_subdistrict_usecase.dart';
import 'package:core_modules/regions/search/presentation/bloc/search_location_by_subdistrict_bloc.dart';
import 'package:get_it/get_it.dart';

class SearchLocationInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies() {
    RegionsInjection.registerGeneralRepository();
    _getIt.registerFactory<GetLocationBySubdistrictUsecase>(
        () => GetLocationBySubdistrictUsecase(_getIt()));
    _getIt.registerFactory<SearchLocationBySubdistrictBloc>(
        () => SearchLocationBySubdistrictBloc(_getIt()));
  }

  static void unRegisterDependencies() {
    _getIt.unregister<GetLocationBySubdistrictUsecase>();
    _getIt.unregister<SearchLocationBySubdistrictBloc>();
  }

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }
}
