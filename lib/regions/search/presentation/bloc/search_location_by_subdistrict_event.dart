import 'package:core_bloc/base_state.dart';

abstract class LocationBySearchEvent<T> extends BaseUiUIEvent<T> {}

class FindLocationBySubdistrictEvent extends LocationBySearchEvent<String>
    with UiEventParam<String> {
  FindLocationBySubdistrictEvent(this.param);

  @override
  final String param;
}
