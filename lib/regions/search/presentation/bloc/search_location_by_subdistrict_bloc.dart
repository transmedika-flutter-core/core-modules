import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/bloc.dart';
import 'package:core_model/entities/location.dart';
import 'package:core_modules/regions/search/domain/usecases/get_location_by_subdistrict_usecase.dart';
import 'package:core_modules/regions/search/presentation/bloc/search_location_by_subdistrict_event.dart';
import 'package:core_modules/regions/search/presentation/bloc/search_location_by_subdistrict_state.dart';

class SearchLocationBySubdistrictBloc
    extends MyBaseBloc<LocationBySearchEvent, SearchLocationBySubdistrictUIState> {
  SearchLocationBySubdistrictBloc(GetLocationBySubdistrictUsecase getLocationBySubdistrictUsecase)
      : super(SearchLocationBySubdistrictUIState()) {
    onRequestUsecase<FindLocationBySubdistrictEvent, List<LocationEntity>, String>(
        getLocationBySubdistrictUsecase, onCheckRequestCondition: (event, state, emitter) {
      emitter(state.copyWith(search: event.param));
      return event.param.length >= 3;
    }, transformer: debounceDroppable(dropDuration));
  }
}
