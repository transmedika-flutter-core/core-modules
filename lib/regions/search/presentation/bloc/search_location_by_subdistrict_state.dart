import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/entities/location.dart';

class SearchLocationBySubdistrictState extends RequestState<List<LocationEntity>> {
  SearchLocationBySubdistrictState({
    this.data = const [],
    this.requestStatus = RequestStatus.idle,
    this.exception,
  });

  @override
  final List<LocationEntity> data;

  @override
  final Exception? exception;

  @override
  final RequestStatus requestStatus;

  @override
  SearchLocationBySubdistrictState copyWith(
      {List<LocationEntity>? data,
      RequestStatus? requestStatus,
      Exception? exception}) {
    return SearchLocationBySubdistrictState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception);
  }

  @override
  List<Object?> get props => [data, requestStatus, exception];
}

class SearchLocationBySubdistrictUIState
    extends BaseUiState<List<LocationEntity>, SearchLocationBySubdistrictState> {
  SearchLocationBySubdistrictUIState(
      {SearchLocationBySubdistrictState? requestState, this.search = ""})
      : requestState = requestState ?? SearchLocationBySubdistrictState();

  @override
  final SearchLocationBySubdistrictState requestState;
  final String search;

  @override
  SearchLocationBySubdistrictUIState copyWith(
      {SearchLocationBySubdistrictState? requestState, String? search}) {
    return SearchLocationBySubdistrictUIState(
      requestState: requestState ?? this.requestState,
      search: search ?? this.search,
    );
  }

  @override
  List<Object?> get props => [requestState, search];
}
