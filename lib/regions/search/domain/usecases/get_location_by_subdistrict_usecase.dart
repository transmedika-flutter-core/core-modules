import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/entities/location.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';

class GetLocationBySubdistrictUsecase
    implements UseCaseService<List<LocationEntity>, String> {
  GetLocationBySubdistrictUsecase(GeneralRepositoryService repository)
      : _repository = repository;

  final GeneralRepositoryService _repository;

  @override
  Future<Result<List<LocationEntity>>> call(String param) {
    return _repository.getLocationsBySubDistrict(param);
  }
}
