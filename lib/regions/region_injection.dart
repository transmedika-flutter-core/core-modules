import 'package:core_data/general/general_repository.dart';
import 'package:core_data/general/general_repository_service.dart';
import 'package:core_modules/regions/district/domain/usecases/district_usecase.dart';
import 'package:core_modules/regions/district/domain/usecases/district_usecase_service.dart';
import 'package:core_modules/regions/district/presentation/bloc/district_bloc.dart';
import 'package:core_modules/regions/province/domain/usecases/province_usecase.dart';
import 'package:core_modules/regions/province/domain/usecases/province_usecase_service.dart';
import 'package:core_modules/regions/province/presentation/bloc/province_bloc.dart';
import 'package:core_modules/regions/regency/domain/usecases/regency_usecase.dart';
import 'package:core_modules/regions/regency/domain/usecases/regency_usecase_service.dart';
import 'package:core_modules/regions/regency/presentation/bloc/regency_bloc.dart';
import 'package:core_modules/regions/sub_district/domain/usecases/sub_district_usecase.dart';
import 'package:core_modules/regions/sub_district/domain/usecases/sub_district_usecase_service.dart';
import 'package:core_modules/regions/sub_district/presentation/bloc/sub_district_bloc.dart';
import 'package:core_network/network_data_source_service.dart';
import 'package:get_it/get_it.dart';

class RegionsInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies({String? instanceName}) {
    registerGeneralRepository(instanceName: instanceName);
    registerProvincesDependencies(instanceName: instanceName);
    registerRegenciesDependencies(instanceName: instanceName);
    registerDistrictsDependencies(instanceName: instanceName);
    registerSubDistrictsDependencies(instanceName: instanceName);
  }

  static void registerGeneralRepository({String? instanceName}) {
    if (!_getIt.isRegistered<GeneralRepositoryService>(
        instanceName: instanceName)) {
      _getIt.registerSingleton<GeneralRepositoryService>(
        GeneralRepository(
            networkDataSourceService: _getIt<NetworkDataSourceService>()),
        instanceName: instanceName,
      );
    }
  }

  static void registerProvincesDependencies({String? instanceName}) {
    if (!_getIt.isRegistered<ProvinceUseCaseService>(
        instanceName: instanceName)) {
      _getIt.registerFactory<ProvinceUseCaseService>(
        () => ProvinceUseCase(
            generalRepositoryService: _getIt<GeneralRepositoryService>()),
        instanceName: instanceName,
      );
    }

    if (!_getIt.isRegistered<ProvinceBloc>(instanceName: instanceName)) {
      _getIt.registerFactory<ProvinceBloc>(
        () => ProvinceBloc(
            provinceUseCaseService: _getIt<ProvinceUseCaseService>()),
        instanceName: instanceName,
      );
    }
  }

  static void registerRegenciesDependencies({String? instanceName}) {
    if (!_getIt.isRegistered<RegencyUseCaseService>(
        instanceName: instanceName)) {
      _getIt.registerFactory<RegencyUseCaseService>(
        () => RegencyUseCase(
            generalRepositoryService: _getIt<GeneralRepositoryService>()),
        instanceName: instanceName,
      );
    }

    if (!_getIt.isRegistered<RegencyBloc>(instanceName: instanceName)) {
      _getIt.registerFactory<RegencyBloc>(
        () =>
            RegencyBloc(regencyUseCaseService: _getIt<RegencyUseCaseService>()),
        instanceName: instanceName,
      );
    }
  }

  static void registerDistrictsDependencies({String? instanceName}) {
    if (!_getIt.isRegistered<DistrictUseCaseService>(
        instanceName: instanceName)) {
      _getIt.registerFactory<DistrictUseCaseService>(
        () => DistrictUseCase(
            generalRepositoryService: _getIt<GeneralRepositoryService>()),
        instanceName: instanceName,
      );
    }

    if (!_getIt.isRegistered<DistrictBloc>(instanceName: instanceName)) {
      _getIt.registerFactory<DistrictBloc>(
        () => DistrictBloc(
            districtUseCaseService: _getIt<DistrictUseCaseService>()),
        instanceName: instanceName,
      );
    }
  }

  static void registerSubDistrictsDependencies({String? instanceName}) {
    if (!_getIt.isRegistered<SubDistrictUseCaseService>(
        instanceName: instanceName)) {
      _getIt.registerFactory<SubDistrictUseCaseService>(
        () => SubDistrictUseCase(
            generalRepositoryService: _getIt<GeneralRepositoryService>()),
        instanceName: instanceName,
      );
    }

    if (!_getIt.isRegistered<SubDistrictBloc>(instanceName: instanceName)) {
      _getIt.registerFactory<SubDistrictBloc>(
        () => SubDistrictBloc(
            subDistrictUseCaseService: _getIt<SubDistrictUseCaseService>()),
        instanceName: instanceName,
      );
    }
  }

  static void unRegisterDependencies({String? instanceName}) {
    unRegisterGeneralRepository(instanceName: instanceName);
    unRegisterProvincesDependencies(instanceName: instanceName);
    unRegisterRegenciesDependencies(instanceName: instanceName);
    unRegisterDistrictsDependencies(instanceName: instanceName);
    unRegisterSubDistrictsDependencies(instanceName: instanceName);
  }

  static void unRegisterGeneralRepository({String? instanceName}) {
    if (_getIt.isRegistered<GeneralRepositoryService>(
        instanceName: instanceName)) {
      _getIt.unregister<GeneralRepositoryService>(instanceName: instanceName);
    }
  }

  static void unRegisterProvincesDependencies({String? instanceName}) {
    if (_getIt.isRegistered<ProvinceUseCaseService>(
        instanceName: instanceName)) {
      _getIt.unregister<ProvinceUseCaseService>(instanceName: instanceName);
    }

    if (_getIt.isRegistered<ProvinceBloc>(instanceName: instanceName)) {
      _getIt.unregister<ProvinceBloc>(instanceName: instanceName);
    }
  }

  static void unRegisterRegenciesDependencies({String? instanceName}) {
    if (_getIt.isRegistered<RegencyUseCaseService>(
        instanceName: instanceName)) {
      _getIt.unregister<RegencyUseCaseService>(instanceName: instanceName);
    }

    if (_getIt.isRegistered<RegencyBloc>(instanceName: instanceName)) {
      _getIt.unregister<RegencyBloc>(instanceName: instanceName);
    }
  }

  static void unRegisterDistrictsDependencies({String? instanceName}) {
    if (_getIt.isRegistered<DistrictUseCaseService>(
        instanceName: instanceName)) {
      _getIt.unregister<DistrictUseCaseService>(instanceName: instanceName);
    }

    if (_getIt.isRegistered<DistrictBloc>(instanceName: instanceName)) {
      _getIt.unregister<DistrictBloc>(instanceName: instanceName);
    }
  }

  static void unRegisterSubDistrictsDependencies({String? instanceName}) {
    if (_getIt.isRegistered<SubDistrictUseCaseService>(
        instanceName: instanceName)) {
      _getIt.unregister<SubDistrictUseCaseService>(instanceName: instanceName);
    }

    if (_getIt.isRegistered<SubDistrictBloc>(instanceName: instanceName)) {
      _getIt.unregister<SubDistrictBloc>(instanceName: instanceName);
    }
  }

  static T get<T extends Object>({String? instanceName}) {
    return _getIt.get<T>(instanceName: instanceName);
  }
}
