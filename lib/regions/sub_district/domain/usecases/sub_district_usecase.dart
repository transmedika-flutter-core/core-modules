import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/sub_district/domain/usecases/sub_district_usecase_service.dart';
import 'package:core_network/base/base_usecase.dart';

class SubDistrictUseCase extends BaseUseCase implements SubDistrictUseCaseService{
  final GeneralRepositoryService generalRepositoryService;

  SubDistrictUseCase({required this.generalRepositoryService});

  @override
  Future<Result<List<BaseSelect>>> getSubDistrictByDistrictId(int districId) async{
    Result<List<BaseSelect>> resp = await getBaseResponseData<List<BaseSelect>>(generalRepositoryService.getSubDistrictsByDistrictId(districId));
    return resp;
  }
}