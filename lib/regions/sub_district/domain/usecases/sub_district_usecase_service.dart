import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';

abstract class SubDistrictUseCaseService{
  Future<Result<List<BaseSelect>>> getSubDistrictByDistrictId(int districId);
}