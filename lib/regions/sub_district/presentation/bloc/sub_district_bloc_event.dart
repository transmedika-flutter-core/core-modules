import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class SubDistricUIEvent extends BaseUiUIEvent{}

class OnSubDistrictRequest extends SubDistricUIEvent {
  OnSubDistrictRequest({required this.districtId, this.defaultValue});
  final int? districtId;
  final BaseSelect? defaultValue;
}