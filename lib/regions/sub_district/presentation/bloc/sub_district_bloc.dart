import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/sub_district/domain/usecases/sub_district_usecase_service.dart';
import 'package:core_modules/regions/sub_district/presentation/bloc/sub_district_bloc_event.dart';
import 'package:core_modules/regions/sub_district/presentation/bloc/sub_district_bloc_state.dart';
import 'package:flutter/foundation.dart';

class SubDistrictBloc extends MyBaseBloc<SubDistricUIEvent, SubDistrictUIState>{
  SubDistrictBloc({required SubDistrictUseCaseService subDistrictUseCaseService}): _subDistrictUseCaseService = subDistrictUseCaseService,
        super(SubDistrictUIState(
        requestState: SubDistrictState(),
      )) {
    onRequest<OnSubDistrictRequest, List<BaseSelect>>(
      initState: (event, state) {
        return state.copyWith(dataSelected:  Wrapped.value(event.defaultValue));
      },
      onRequest: (event, state) {
        return _subDistrictUseCaseService
            .getSubDistrictByDistrictId(event.districtId!);
      },
    );
  }

  final SubDistrictUseCaseService _subDistrictUseCaseService;

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}