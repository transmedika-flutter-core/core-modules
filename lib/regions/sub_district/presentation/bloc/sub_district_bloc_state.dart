import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_select.dart';

class SubDistrictState extends RequestState<List<BaseSelect>>{
  @override final List<BaseSelect>? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  SubDistrictState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  SubDistrictState copyWith({
    List<BaseSelect>? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return SubDistrictState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class SubDistrictUIState extends BaseUiState<List<BaseSelect>, SubDistrictState>{
 SubDistrictUIState({
    this.requestState,
    this.dataSelected
  });

  @override
  final SubDistrictState? requestState;
  final BaseSelect? dataSelected;


  @override
  SubDistrictUIState copyWith({
    SubDistrictState? requestState,
    Wrapped<BaseSelect?>? dataSelected
  }) {
    return SubDistrictUIState(
        requestState: requestState ?? this.requestState,
      dataSelected: dataSelected!=null ? dataSelected.value : this.dataSelected,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    dataSelected
  ];
}