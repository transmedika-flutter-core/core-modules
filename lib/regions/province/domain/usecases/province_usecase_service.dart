import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';

abstract class ProvinceUseCaseService{
  Future<Result<List<BaseSelect>>> getProvince();
}