import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/province/domain/usecases/province_usecase_service.dart';
import 'package:core_network/base/base_usecase.dart';

class ProvinceUseCase extends BaseUseCase implements ProvinceUseCaseService{
  final GeneralRepositoryService generalRepositoryService;

  ProvinceUseCase({required this.generalRepositoryService});

  @override
  Future<Result<List<BaseSelect>>> getProvince() async{
    Result<List<BaseSelect>> resp = await getBaseResponseData<List<BaseSelect>>(generalRepositoryService.getProvincies());
    return resp;
  }
}