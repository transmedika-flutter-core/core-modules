import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_select.dart';

class ProvinceState extends RequestState<List<BaseSelect>>{
  @override final List<BaseSelect>? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  ProvinceState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  ProvinceState copyWith({
    List<BaseSelect>? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return ProvinceState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class ProvinceUIState extends BaseUiState<List<BaseSelect>, ProvinceState>{
  ProvinceUIState({
    this.requestState,
    this.dataSelected
  });

  @override
  final ProvinceState? requestState;
  final BaseSelect? dataSelected;


  @override
  ProvinceUIState copyWith({
    ProvinceState? requestState,
    BaseSelect? dataSelected
  }) {
    return ProvinceUIState(
        requestState: requestState ?? this.requestState,
        dataSelected: dataSelected ?? this.dataSelected,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    dataSelected
  ];
}