import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/province/domain/usecases/province_usecase_service.dart';
import 'package:core_modules/regions/province/presentation/bloc/province_bloc_event.dart';
import 'package:core_modules/regions/province/presentation/bloc/province_bloc_state.dart';
import 'package:flutter/foundation.dart';

class ProvinceBloc extends MyBaseBloc<ProvinceUIEvent, ProvinceUIState>{
  ProvinceBloc({required ProvinceUseCaseService provinceUseCaseService}): _provinceUseCaseService = provinceUseCaseService,
        super(ProvinceUIState(
        requestState: ProvinceState(),
      )) {
    onRequest<OnProvinceRequest, List<BaseSelect>>(
      initState: (event, state) {
        return state.copyWith(dataSelected: event.defaultValue);
      },
      onRequest: (event, state) {
        return _provinceUseCaseService.getProvince();
      },
    );
  }

  final ProvinceUseCaseService _provinceUseCaseService;

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}