import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class DistricUIEvent extends BaseUiUIEvent{}

class OnDistrictRequest extends DistricUIEvent {
  OnDistrictRequest({required this.regencyId, this.defaultValue});
  final int? regencyId;
  final BaseSelect? defaultValue;
}