import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/district/domain/usecases/district_usecase_service.dart';
import 'package:core_modules/regions/district/presentation/bloc/district_bloc_event.dart';
import 'package:core_modules/regions/district/presentation/bloc/district_bloc_state.dart';
import 'package:flutter/foundation.dart';

class DistrictBloc extends MyBaseBloc<DistricUIEvent, DistrictUIState>{
  DistrictBloc({required DistrictUseCaseService districtUseCaseService}): _districtUseCaseService = districtUseCaseService,
        super(DistrictUIState(
        requestState: DistrictState(),
      )) {
    onRequest<OnDistrictRequest, List<BaseSelect>>(
      initState: (event, state) {
        return state.copyWith(dataSelected:  Wrapped.value(event.defaultValue));
      },
      onRequest: (event, state) {
        return _districtUseCaseService.getDistrictByRegencyId(event.regencyId!);
      },
    );
  }

  final DistrictUseCaseService _districtUseCaseService;

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}