import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_select.dart';

class DistrictState extends RequestState<List<BaseSelect>>{
  @override final List<BaseSelect>? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  DistrictState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  DistrictState copyWith({
    List<BaseSelect>? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return DistrictState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class DistrictUIState extends BaseUiState<List<BaseSelect>, DistrictState>{
  DistrictUIState({
    this.requestState,
    this.dataSelected
  });

  @override
  final DistrictState? requestState;
  final BaseSelect? dataSelected;


  @override
  DistrictUIState copyWith({
    DistrictState? requestState,
    Wrapped<BaseSelect?>? dataSelected
  }) {
    return DistrictUIState(
        requestState: requestState ?? this.requestState,
      dataSelected: dataSelected!=null ? dataSelected.value : this.dataSelected,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    dataSelected
  ];
}