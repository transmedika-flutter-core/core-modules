import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/district/domain/usecases/district_usecase_service.dart';
import 'package:core_network/base/base_usecase.dart';

class DistrictUseCase extends BaseUseCase implements DistrictUseCaseService{
  final GeneralRepositoryService generalRepositoryService;

  DistrictUseCase({required this.generalRepositoryService});

  @override
  Future<Result<List<BaseSelect>>> getDistrictByRegencyId(int regencyId) async{
    Result<List<BaseSelect>> resp = await getBaseResponseData<List<BaseSelect>>(generalRepositoryService.getDistrictsByRegencyId(regencyId));
    return resp;
  }
}