import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/regency/domain/usecases/regency_usecase_service.dart';
import 'package:core_modules/regions/regency/presentation/bloc/regency_bloc_event.dart';
import 'package:core_modules/regions/regency/presentation/bloc/regency_bloc_state.dart';
import 'package:flutter/foundation.dart';

class RegencyBloc extends MyBaseBloc<RegencyUIEvent, RegencyUIState>{
  RegencyBloc({required RegencyUseCaseService regencyUseCaseService}): _regencyUseCaseService = regencyUseCaseService,
        super(RegencyUIState(
        requestState: RegencyState(),
      )) {
    onRequest<OnRegencyRequest, List<BaseSelect>>(
      initState: (event, state) {
        return state.copyWith(dataSelected:  Wrapped.value(event.defaultValue));
      },
      onRequest: (event, state) {
        return _regencyUseCaseService.getRegenciesByProvinceId(event.provinceId!);
      },
    );
  }

  final RegencyUseCaseService _regencyUseCaseService;

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}