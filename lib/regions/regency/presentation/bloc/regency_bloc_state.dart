import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_select.dart';

class RegencyState extends RequestState<List<BaseSelect>>{
  @override final List<BaseSelect>? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  RegencyState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  RegencyState copyWith({
    List<BaseSelect>? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return RegencyState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class RegencyUIState extends BaseUiState<List<BaseSelect>, RegencyState>{
  RegencyUIState({
    this.requestState,
    this.dataSelected
  });

  @override
  final RegencyState? requestState;
  final BaseSelect? dataSelected;


  @override
  RegencyUIState copyWith({
    RegencyState? requestState,
    Wrapped<BaseSelect?>? dataSelected
  }) {
    return RegencyUIState(
        requestState: requestState ?? this.requestState,
      dataSelected: dataSelected!=null ? dataSelected.value : this.dataSelected,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    dataSelected
  ];
}