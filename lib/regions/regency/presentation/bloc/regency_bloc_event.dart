import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class RegencyUIEvent extends BaseUiUIEvent{}

class OnRegencyRequest extends RegencyUIEvent {
  OnRegencyRequest({required this.provinceId, this.defaultValue});
  final int? provinceId;
  final BaseSelect? defaultValue;
}