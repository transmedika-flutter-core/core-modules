import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';

abstract class RegencyUseCaseService{
  Future<Result<List<BaseSelect>>> getRegenciesByProvinceId(int provinceId);
}