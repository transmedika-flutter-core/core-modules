import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_modules/regions/regency/domain/usecases/regency_usecase_service.dart';
import 'package:core_network/base/base_usecase.dart';

class RegencyUseCase extends BaseUseCase implements RegencyUseCaseService{
  final GeneralRepositoryService generalRepositoryService;

  RegencyUseCase({required this.generalRepositoryService});

  @override
  Future<Result<List<BaseSelect>>> getRegenciesByProvinceId(int provinceId) async{
    Result<List<BaseSelect>> resp = await getBaseResponseData<List<BaseSelect>>(generalRepositoryService.getRegenciesByProvinceId(provinceId));
    return resp;
  }
}