import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';

abstract class CountryUseCaseService{
  Future<Result<List<BaseSelect>>> getCountries(String? search);
}