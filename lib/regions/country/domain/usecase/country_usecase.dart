import 'package:core_data/general/general_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:core_network/base/base_usecase.dart';

import 'country_usecase_service.dart';

class CountryUseCase extends BaseUseCase implements CountryUseCaseService{
  final GeneralRepositoryService generalRepositoryService;

  CountryUseCase({required this.generalRepositoryService});

  @override
  Future<Result<List<BaseSelect>>> getCountries(String? search) async{
    Result<List<BaseSelect>> resp = await getBaseResponseData<List<BaseSelect>>(generalRepositoryService.getCountries(search));
    return resp;
  }
}