import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_select.dart';

class CountryState extends RequestState<List<BaseSelect>>{
  @override final List<BaseSelect>? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  CountryState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  CountryState copyWith({
    List<BaseSelect>? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return CountryState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class CountryUIState extends BaseUiState<List<BaseSelect>, CountryState>{
  CountryUIState({
    this.requestState,
    this.dataSelected
  });

  @override
  final CountryState? requestState;
  final BaseSelect? dataSelected;


  @override
  CountryUIState copyWith({
    CountryState? requestState,
    BaseSelect? dataSelected
  }) {
    return CountryUIState(
        requestState: requestState ?? this.requestState,
        dataSelected: dataSelected ?? this.dataSelected,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    dataSelected
  ];
}