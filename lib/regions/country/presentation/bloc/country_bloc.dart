import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:flutter/foundation.dart';

import '../../domain/usecase/country_usecase_service.dart';
import 'country_bloc_event.dart';
import 'country_bloc_state.dart';

class CountryBloc extends MyBaseBloc<CountryUIEvent, CountryUIState>{
  CountryBloc({required CountryUseCaseService countryUseCaseService}): _countryUseCaseService = countryUseCaseService,
        super(CountryUIState(
        requestState:CountryState(),
      )) {
    onRequest<OnCountryRequest, List<BaseSelect>>(
      initState: (event, state) {
        return state.copyWith(dataSelected: event.defaultValue);
      },
      onRequest: (event, state) {
        return _countryUseCaseService.getCountries(null);
      },
    );
  }

  final CountryUseCaseService _countryUseCaseService;

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}