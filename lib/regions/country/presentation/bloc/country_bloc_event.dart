import 'package:core_bloc/base_state.dart';
import 'package:core_model/network/base/base_select.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class CountryUIEvent extends BaseUiUIEvent{}

class OnCountryRequest extends CountryUIEvent {
  OnCountryRequest({this.defaultValue});
  final BaseSelect? defaultValue;
}