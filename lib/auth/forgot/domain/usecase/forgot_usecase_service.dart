import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/param/forgot_param.dart';

abstract class ForgotUseCaseService{
  Future<Result<dynamic>> postForgot(ForgotParam param);
}