import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_network/base/base_usecase.dart';

import 'forgot_usecase_service.dart';

class ForgotUseCase extends BaseUseCase implements ForgotUseCaseService{
  final AuthRepositoryService authRepositoryService;

  ForgotUseCase({required this.authRepositoryService});

  @override
  Future<Result> postForgot(ForgotParam param) async {
    Result<dynamic> resp = await getBaseResponseData<dynamic>(authRepositoryService.postForgot(param));
    return resp;
  }
}