import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';

class ForgotState extends RequestState<dynamic>{
  @override final dynamic data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  ForgotState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  ForgotState copyWith({
    dynamic data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return ForgotState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class ForgotUIState extends BaseUiState<dynamic, ForgotState>{
  ForgotUIState({
    this.requestState,
    this.email
  });

  @override
  final ForgotState? requestState;
  final String? email;


  @override
  ForgotUIState copyWith({
    ForgotState? requestState,
    Wrapped<String?>? email,
  }) {
    return ForgotUIState(
        requestState: requestState ?? this.requestState,
      email:  email != null ? email.value : this.email,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    email
  ];
}