import 'package:core_bloc/base_state.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class ForgotUIEvent extends BaseUiUIEvent{}


class OnForgotRequest extends ForgotUIEvent {}
class OnForgotIdle extends ForgotUIEvent {}

class OnInsertForgotParam extends ForgotUIEvent {
  OnInsertForgotParam(this.text);
  final String? text;
}
