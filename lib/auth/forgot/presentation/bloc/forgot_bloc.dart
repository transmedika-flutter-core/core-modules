import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/bloc.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/usecase/forgot_usecase_service.dart';
import 'forgot_bloc_event.dart';
import 'forgot_bloc_state.dart';

class ForgotBloc extends MyBaseBloc<ForgotUIEvent, ForgotUIState>{
  ForgotBloc({required ForgotUseCaseService forgotUseCaseService}): _forgotUseCaseService = forgotUseCaseService,
        super(ForgotUIState(
        requestState: ForgotState(),
      )) {

    on<OnForgotIdle>(_onForgotIdle);
    on<OnForgotRequest>(_onForgotRequested, transformer: throttleDroppable(const Duration(milliseconds: 2000)));
    on<OnInsertForgotParam>(_onInsertForgotParam);
  }

  final ForgotUseCaseService _forgotUseCaseService;

  _onInsertForgotParam(OnInsertForgotParam event, Emitter<ForgotUIState> emit) async{
      emit(state.copyWith(email: Wrapped.value(event.text)));
  }


  _onForgotRequested(OnForgotRequest event, Emitter<ForgotUIState> emit) async{
    emit(state.copyWith(requestState: ForgotState(requestStatus: RequestStatus.loading)));
    Result<dynamic> forgot = await _forgotUseCaseService.postForgot(ForgotParam(state.email!));
    if(forgot is ResultSuccess){
      emit(state.copyWith(
          requestState: ForgotState(requestStatus: RequestStatus.success, data: forgot.data)));
    }else{
      emit(state.copyWith(
          requestState: ForgotState(requestStatus: RequestStatus.failure, exception: forgot.error)));
    }
  }

  _onForgotIdle(OnForgotIdle event, Emitter<ForgotUIState> emit) async{
    emit(state.copyWith(
        requestState: ForgotState(requestStatus: RequestStatus.idle)));
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}