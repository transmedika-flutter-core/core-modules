import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/bloc.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';
import 'package:core_modules/auth/signin/bloc/signin_event.dart';
import 'package:core_modules/auth/signin/bloc/signin_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../domain/usecase/signin_usecase_service.dart';

class SignInBloc extends MyBaseBloc<SignInUIEvent, SignInUIState>{
  SignInBloc({required SignInUseCaseService signInUseCaseService}): _signInUseCaseService = signInUseCaseService,
        super(SignInUIState(
        requestState: SignInState(),
      )) {

    on<OnSignInIdle>(_onSignInIdle);
    on<OnEmailChange>(_onEmailChange);
    on<OnPasswordChange>(_onPasswordChange);
    on<OnKeepMeSignedIn>(_onKeepMeSignedIn);
    on<OnTokenIdChange>(_onTokenIdChange);
    on<OnCheckKeepMeSignedIn>(_onCheckKeepMeSignedIn);
    onRequest<OnSignInRequest, SignIn>(
        onRequest: (event, state) {
          if(state.keepSignedIn !=null && state.email!= null && state.password !=null) {
            KeepSignedIn keepSignedIn = KeepSignedIn(
                state.email, state.password, state.refType);
            _signInUseCaseService.keepSignedIn(keepSignedIn);
          }
          return _signInUseCaseService.postSignIn(SignInParam(state.email!, state.password!, event.refType!, tokenId: state.tokenId));
        },
        transformer: throttleDroppable(const Duration(milliseconds: 2000))
    );
  }

  final SignInUseCaseService _signInUseCaseService;

  _onCheckKeepMeSignedIn(OnCheckKeepMeSignedIn event, Emitter<SignInUIState> emit) async{
    KeepSignedIn? keepSignedIn = await _signInUseCaseService.getKeepSignedIn();
    if(keepSignedIn!=null) {
      emit(state.copyWith(
          email: Wrapped.value(keepSignedIn.userName),
          password: Wrapped.value(keepSignedIn.password),
          refType: Wrapped.value(keepSignedIn.refType),
          keepSignedIn: Wrapped.value(keepSignedIn))
      );
    }
  }

  _onKeepMeSignedIn(OnKeepMeSignedIn event, Emitter<SignInUIState> emit) async{
    if(event.checked == true && state.email!= null && state.password !=null){
      KeepSignedIn keepSignedIn = KeepSignedIn(state.email, state.password, state.refType);
      _signInUseCaseService.keepSignedIn(keepSignedIn);
      emit(state.copyWith(keepSignedIn: Wrapped.value(keepSignedIn)));
    }else{
      _signInUseCaseService.removekeepSignedIn();
      emit(state.copyWith(keepSignedIn: const Wrapped.value(null)));
    }
  }

  _onEmailChange(OnEmailChange event, Emitter<SignInUIState> emit) async{
    emit(state.copyWith(email: Wrapped.value(event.email)));
  }

  _onPasswordChange(OnPasswordChange event, Emitter<SignInUIState> emit) async{
    emit(state.copyWith(password: Wrapped.value(event.password)));
  }

  _onSignInIdle(OnSignInIdle event, Emitter<SignInUIState> emit) async{
    emit(state.copyWith(
        requestState: SignInState(requestStatus: RequestStatus.idle)));
  }

  _onTokenIdChange(OnTokenIdChange event, Emitter<SignInUIState> emit) async {
    emit(state.copyWith(tokenId: Wrapped.value(event.tokenId)));
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}