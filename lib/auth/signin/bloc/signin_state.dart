import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';

class SignInState extends RequestState<SignIn>{
  @override final SignIn? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  SignInState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  RequestState copyWith({
    SignIn? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return SignInState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class SignInUIState extends BaseUiState<SignIn, SignInState>{
  SignInUIState({
    this.requestState,
    this.email,
    this.password,
    this.refType,
    this.tokenId,
    this.keepSignedIn,
  });

  @override
  final SignInState? requestState;
  final String? email;
  final String? password;
  final String? refType;
  final String? tokenId;
  final KeepSignedIn? keepSignedIn;

  @override
  SignInUIState copyWith({
    SignInState? requestState,
    Wrapped<String?>? email,
    Wrapped<String?>? password,
    Wrapped<String?>? refType,
    Wrapped<String?>? tokenId,
    Wrapped<KeepSignedIn?>? keepSignedIn
  }) {
    return SignInUIState(
        requestState: requestState ?? this.requestState,
        email:  email != null ? email.value : this.email,
        password:  password != null ? password.value : this.password,
        refType:  refType != null ? refType.value : this.refType,
        tokenId:  tokenId != null ? tokenId.value : this.tokenId,
        keepSignedIn:  keepSignedIn != null ? keepSignedIn.value : this.keepSignedIn
    );
  }

  bool valid() => this.email!=null && this.password!=null;

  @override
  List<Object?> get props => [
    requestState,
    email,
    password,
    refType,
    tokenId,
    keepSignedIn
  ];
}