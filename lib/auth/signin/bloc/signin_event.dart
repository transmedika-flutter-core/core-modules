import 'package:core_bloc/base_state.dart';
import 'package:flutter/cupertino.dart';


@immutable
abstract class SignInUIEvent extends BaseUiUIEvent{}

class OnSignInRequest extends SignInUIEvent {
  OnSignInRequest({required this.refType});
  final String? refType;
}
class OnSignInIdle extends SignInUIEvent {}

class OnEmailChange extends SignInUIEvent {
  OnEmailChange({required this.email});
  final String? email;
}

class OnPasswordChange extends SignInUIEvent {
  OnPasswordChange({required this.password});
  final String? password;
}

class OnTokenIdChange extends SignInUIEvent {
  OnTokenIdChange({required this.tokenId});
  final String? tokenId;
}

class OnKeepMeSignedIn extends SignInUIEvent {
  OnKeepMeSignedIn({required this.checked});
  final bool? checked;
}

class OnCheckKeepMeSignedIn extends SignInUIEvent {}