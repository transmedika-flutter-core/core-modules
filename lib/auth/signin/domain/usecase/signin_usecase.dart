import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';
import 'package:core_modules/auth/signin/domain/usecase/signin_usecase_service.dart';
import 'package:core_network/base/base_usecase.dart';

class SignInUseCase extends BaseUseCase implements SignInUseCaseService{
  final AuthRepositoryService authRepositoryService;

  SignInUseCase({required this.authRepositoryService});

  @override
  Future<Result<SignIn>> postSignIn(SignInParam param) async {
    Result<SignIn> resp = await getBaseResponseData<SignIn>(authRepositoryService.postSignIn(param));
    if(resp.data!=null) {
      await authRepositoryService.saveSession(resp.data!);
    }
    return resp;
  }

  @override
  Future<KeepSignedIn?> getKeepSignedIn() async{
    KeepSignedIn? keepSignedIn = await authRepositoryService.getKeepSignedIn();
    return keepSignedIn;
  }

  @override
  Future<void> keepSignedIn(KeepSignedIn keepSignedIn) {
    return authRepositoryService.keepSignedIn(keepSignedIn);
  }

  @override
  Future<void> removekeepSignedIn() => authRepositoryService.removekeepSignedIn();
}