import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';

abstract class SignInUseCaseService{
  Future<Result<SignIn>> postSignIn(SignInParam param);
  Future<void> keepSignedIn(KeepSignedIn keepSignedIn);
  Future<void> removekeepSignedIn();
  Future<KeepSignedIn?> getKeepSignedIn();
}