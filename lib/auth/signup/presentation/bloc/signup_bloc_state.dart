import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/response/signin.dart';

class SignUpState extends RequestState<SignIn>{
  @override final SignIn? data;
  @override final RequestStatus requestStatus;
  @override final Exception? exception;

  SignUpState({
    this.data,
    this.requestStatus = RequestStatus.idle,
    this.exception
  });

  @override
  RequestState copyWith({
    SignIn? data,
    RequestStatus? requestStatus,
    Exception? exception
  }) {
    return SignUpState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception
    );
  }
}

class SignUpUIState extends BaseUiState<SignIn, SignUpState>{
  SignUpUIState({
    this.requestState,
    this.fullName,
    this.email,
    this.phoneNumber,
    this.password,
    this.retypePassword
  });

  @override
  final SignUpState? requestState;
  final String? fullName;
  final String? email;
  final String? phoneNumber;
  final String? password;
  final String? retypePassword;

  bool valid(){
    return fullName!=null && email!=null && phoneNumber!=null && password!=null && retypePassword!=null && password == retypePassword;
  }

  @override
  SignUpUIState copyWith({
    SignUpState? requestState,
    Wrapped<String?>? fullName,
    Wrapped<String?>? email,
    Wrapped<String?>? phoneNumber,
    Wrapped<String?>? password,
    Wrapped<String?>? retypePassword,
  }) {
    return SignUpUIState(
        requestState: requestState ?? this.requestState,
        fullName:  fullName!=null ? fullName.value : this.fullName,
        email:  email!=null ? email.value : this.email,
        phoneNumber:  phoneNumber!=null ? phoneNumber.value : this.phoneNumber,
        password:  password!=null ? password.value : this.password,
        retypePassword:  retypePassword!=null ? retypePassword.value : this.retypePassword,
    );
  }

  @override
  List<Object?> get props => [
    requestState,
    fullName,
    email,
    phoneNumber,
    password,
    retypePassword
  ];
}