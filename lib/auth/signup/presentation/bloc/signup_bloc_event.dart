import 'package:core_bloc/base_state.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class SignUpUIEvent extends BaseUiUIEvent{}

enum InsertSignUpParam {fullname, email, phoneNumber, password, retypePassword}

class OnSignUpRequest extends SignUpUIEvent {
  OnSignUpRequest({required this.refType});
  final String? refType;
}

class OnInsertSignUpParam extends SignUpUIEvent {
  OnInsertSignUpParam(this.text, this.insertSignUpParam);
  final String? text;
  final InsertSignUpParam insertSignUpParam;
}