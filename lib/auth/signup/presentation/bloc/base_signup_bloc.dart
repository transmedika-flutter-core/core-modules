import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_modules/auth/signup/presentation/bloc/signup_bloc_event.dart';
import 'package:core_modules/auth/signup/presentation/bloc/signup_bloc_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseSignUpBloc<E extends SignUpUIEvent, S extends BaseUiState<SignIn, SignUpState>>
    extends MyBaseBloc<E,S>{
  BaseSignUpBloc(super.initialState);


  @override
  void onRequest<EX extends E, T>({
    required Future<Result<T>> Function(EX event, S state) onRequest,
    bool Function(EX event, S state, Emitter<S> emitter)? onCheckRequestCondition,
    S Function(EX event, S state)? initState,
    EventTransformer<EX>? transformer,
    Future<S> Function()? onLoading,
    Future<S> Function(T result)? onSuccessResult,
    Future<S> Function(Exception exception)? onErrorResult,
    Function(Exception exception)? onCompleteError,
    Function(EX event,T result)? onCompleteSuccess, Future<S>
    Function(T? result, Exception? exception)? onFinishResult}) {
    super.onRequest(
      onRequest: onRequest,
      transformer: transformer,
      onFinishResult: (result, exception) async {
        var currentState = (state.requestState as RequestState).copyWith(requestStatus: RequestStatus.idle) as SignUpState;
           return state.copyWith(requestState: currentState) as S;
      },
    );
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    if (kDebugMode) {
      print('onError -- bloc: ${error.runtimeType}, error: $error');
    }
    super.onError(error, stackTrace);
  }
}