import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/bloc.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_modules/auth/signup/presentation/bloc/base_signup_bloc.dart';
import 'package:core_modules/auth/signup/presentation/bloc/signup_bloc_event.dart';
import 'package:core_modules/auth/signup/presentation/bloc/signup_bloc_state.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../domain/usecase/base_signup_usecase_service.dart';

class SignUpBloc extends BaseSignUpBloc<SignUpUIEvent, SignUpUIState>{
  SignUpBloc({required SignUpUseCaseService<SignIn,SignUpParam> signUpUseCaseService}): _signUpUseCaseService = signUpUseCaseService,
        super(SignUpUIState(requestState: SignUpState())) {

    on<OnInsertSignUpParam>(_onInsertSignUpParam);
    onRequest<OnSignUpRequest,SignIn>(
      onRequest: (event, state) {
        return _signUpUseCaseService.postSignUp(SignUpParam(
            state.fullName, state.email, state.phoneNumber, null, state.password,state.retypePassword,event.refType
        ));
      },
      transformer: throttleDroppable(const Duration(milliseconds: 2000))
    );
  }

  final SignUpUseCaseService<SignIn,SignUpParam> _signUpUseCaseService;


  _onInsertSignUpParam(OnInsertSignUpParam event, Emitter<SignUpUIState> emit) async{
    if(event.insertSignUpParam == InsertSignUpParam.fullname){
      emit(state.copyWith(fullName: Wrapped.value(event.text)));
    }

    if(event.insertSignUpParam == InsertSignUpParam.email){
      emit(state.copyWith(email: Wrapped.value(event.text)));
    }

    if(event.insertSignUpParam == InsertSignUpParam.phoneNumber){
      emit(state.copyWith(phoneNumber: Wrapped.value(event.text)));
    }

    if(event.insertSignUpParam == InsertSignUpParam.password){
      emit(state.copyWith(password: Wrapped.value(event.text)));
    }

    if(event.insertSignUpParam == InsertSignUpParam.retypePassword){
      emit(state.copyWith(retypePassword: Wrapped.value(event.text)));
    }
  }
}