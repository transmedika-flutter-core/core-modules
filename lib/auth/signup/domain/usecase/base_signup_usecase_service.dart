import 'package:core_model/network/base/base_result.dart';

abstract class SignUpUseCaseService<T,P>{
  Future<Result<T>> postSignUp(P param);
}