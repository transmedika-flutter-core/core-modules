import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_network/base/base_usecase.dart';

import 'base_signup_usecase_service.dart';

class SignUpUseCase extends BaseUseCase
    implements SignUpUseCaseService<SignIn,SignUpParam> {
  final AuthRepositoryService authRepositoryService;

  SignUpUseCase({required this.authRepositoryService});

  @override
  Future<Result<SignIn>> postSignUp(SignUpParam param) async {
    Result<SignIn> resp = await getBaseResponseData<SignIn>(
        authRepositoryService.postSignUp(param));
    return resp;
  }
}
