import 'package:core_data/auth/auth_repository.dart';
import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_modules/auth/signin/bloc/signin_bloc.dart';
import 'package:core_modules/auth/signin/domain/usecase/signin_usecase.dart';
import 'package:core_modules/auth/signin/domain/usecase/signin_usecase_service.dart';
import 'package:core_modules/auth/signup/domain/usecase/base_signup_usecase_service.dart';
import 'package:core_modules/auth/signup/domain/usecase/signup_usecase.dart';
import 'package:core_modules/auth/signup/presentation/bloc/signup_bloc.dart';
import 'package:get_it/get_it.dart';

import 'forgot/domain/usecase/forgot_usecase.dart';
import 'forgot/domain/usecase/forgot_usecase_service.dart';
import 'forgot/presentation/bloc/forgot_bloc.dart';

class AuthInjection{
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies() {
    if(!_getIt.isRegistered<AuthRepositoryService>()) {
      _getIt.registerSingleton<AuthRepositoryService>(
        AuthRepository(
          networkDataSourceService: _getIt(),
          sharedPreferencesDataSourceService: _getIt(),
        ),
      );
    }

    _getIt.registerFactory<SignInUseCaseService>(()=>
        SignInUseCase(
            authRepositoryService: _getIt<AuthRepositoryService>()));

    _getIt.registerFactory<SignUpUseCaseService<SignIn,SignUpParam>>(() =>
        SignUpUseCase(
            authRepositoryService: _getIt<AuthRepositoryService>()));

    _getIt.registerFactory<ForgotUseCaseService>(() =>
        ForgotUseCase(
            authRepositoryService: _getIt<AuthRepositoryService>()));

    _getIt.registerFactory<SignInBloc>(() =>
        SignInBloc(signInUseCaseService: _getIt<SignInUseCaseService>()));

    _getIt.registerFactory<SignUpBloc>(() =>
        SignUpBloc(signUpUseCaseService: _getIt<SignUpUseCaseService<SignIn,SignUpParam>>()));

    _getIt.registerFactory<ForgotBloc>(() =>
        ForgotBloc(forgotUseCaseService: _getIt<ForgotUseCaseService>()));
  }

  static void unRegisterDependencies() {
    if(_getIt.isRegistered<AuthRepositoryService>()) {
      _getIt.unregister<AuthRepositoryService>();
    }
    _getIt.unregister<SignInUseCaseService>();
    _getIt.unregister<SignUpUseCaseService<SignIn,SignUpParam>>();
    _getIt.unregister<ForgotUseCaseService>();
    _getIt.unregister<SignInBloc>();
    _getIt.unregister<SignUpBloc>();
    _getIt.unregister<ForgotBloc>();
  }

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }
}