import 'package:core_data/auth/auth_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';

class LogoutUsecase extends UseCaseService<bool, void> {
  final AuthRepositoryService _repository;

  LogoutUsecase(AuthRepositoryService repository) : _repository = repository;

  @override
  Future<Result<bool>> call([void param]) {
    return _repository.revokeSession();
  }
}
