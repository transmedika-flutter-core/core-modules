import '../network_injection.dart';

void showInspectorAliceIfAvailable() {
  NetworkInjection.getAlice()?.showInspector();
}
