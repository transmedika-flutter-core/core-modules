import 'package:alice/alice.dart';
import 'package:core_network/dio_client.dart';
import 'package:core_network/googleapi/map_google_api_service.dart';
import 'package:core_network/myapi/my_api_service.dart';
import 'package:core_network/network_data_source.dart';
import 'package:core_network/network_data_source_service.dart';
import 'package:core_shared_preferences/shared_preferences_data_source.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class NetworkInjection {
  static final GetIt _getIt = GetIt.instance;
  static const int timeOut = 60000;
  static const String dioMyApiInstance = "myapi-instance";
  static const String dioMapGoogleApiInstance = "mapgoogle-instance";

  static BaseOptions dioBaseOptions() {
    return BaseOptions(
      receiveTimeout: const Duration(milliseconds: timeOut),
      connectTimeout: const Duration(milliseconds: timeOut),
      sendTimeout: const Duration(milliseconds: timeOut),
    );
  }

  static Dio createDioMyApi([Alice? alice]) {
    SharedPreferencesDataSourceService preferencesDataSourceService =
        SharedPreferencesDataSource();
    var dio = Dio(dioBaseOptions());

    final interceptors = [
      NetworkInterceptor(
          preferencesDataSourceService: preferencesDataSourceService),
      PrettyDioLogger(requestBody: true, requestHeader: true)
    ];

    if (alice != null) {
      interceptors.add(alice.getDioInterceptor());
    }

    dio.interceptors.addAll(interceptors);

    return dio;
  }

  static Dio createDioMapGooleApi(
      Alice alice,
      String sessiontoken,
      String key,
      String? iosBundleId,
      String? androidPackage,
      String? androidCert) {
    var dio = Dio(dioBaseOptions());

    dio.interceptors.addAll({
      alice.getDioInterceptor(),
      MapGoogleNetworkInterceptor(
          sessiontoken: sessiontoken,
          key:key,
          iosBundleId: iosBundleId,
          androidPackage: androidPackage,
          androidCert: androidCert),
      PrettyDioLogger(requestBody: true, requestHeader: true)
    });

    return dio;
  }

  static void registerDependencies(
      {required String baseUrl,
      required String sessionToken,
      required String key,
      String? iosBundleId,
      String? androidPackage,
      String? androidCert
      }) {
    if (!_getIt.isRegistered<Alice>()) {
      _getIt.registerSingleton<Alice>(Alice(
        showNotification: kDebugMode ? true : false,
        showInspectorOnShake: kDebugMode ? true : false,
        maxCallsCount: 1000,
      ));
    }

    if (!_getIt.isRegistered<Dio>(instanceName: dioMyApiInstance)) {
      _getIt.registerSingleton<Dio>(createDioMyApi(_getIt<Alice>()),
          instanceName: dioMyApiInstance);
    }
    _getIt.registerSingleton<Dio>(
        createDioMapGooleApi(_getIt<Alice>(), sessionToken, key, iosBundleId, androidPackage, androidCert),
        instanceName: dioMapGoogleApiInstance);
    _getIt.registerSingleton<MyApiService>(MyApiService(
        _getIt<Dio>(instanceName: dioMyApiInstance),
        baseUrl: baseUrl));
    _getIt.registerSingleton<MapGoogleApiService>(MapGoogleApiService(
        _getIt<Dio>(instanceName: dioMapGoogleApiInstance)));
    _getIt.registerSingleton<NetworkDataSourceService>(
      NetworkDataSource(
          myApiService: _getIt<MyApiService>(),
          mapGgoogleApiService: _getIt<MapGoogleApiService>()),
    );
  }

  static void unRegisterDependencies() {
    if (_getIt.isRegistered<Dio>(instanceName: dioMyApiInstance)) {
      _getIt.unregister<Dio>(instanceName: dioMyApiInstance);
    }
    _getIt.unregister<Dio>(instanceName: dioMapGoogleApiInstance);
    _getIt.unregister<MyApiService>();
    _getIt.unregister<MapGoogleApiService>();
    _getIt.unregister<NetworkDataSourceService>();
  }

  static Alice? getAlice() {
    if (_getIt.isRegistered<Alice>()) {
      return get<Alice>();
    }
    return null;
  }

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }
}
