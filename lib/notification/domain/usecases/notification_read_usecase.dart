import 'package:core_model/network/base/base_result.dart';
import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:core_network/base/base_usecase_service.dart';

class NotificationReadUsecase
    implements UseCaseService<int, int> {
  final NotificationRepository _repository;

  NotificationReadUsecase(NotificationRepository repository): _repository = repository;

  @override
  Future<Result<int>> call(int param) {
    return _repository.readNotifications(param);
  }
}
