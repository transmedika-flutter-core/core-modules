import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:core_modules/notification/domain/usecases/notification_add_usecase.dart';
import 'package:core_modules/notification/domain/usecases/notification_get_list_usecase.dart';
import 'package:core_modules/notification/domain/usecases/notification_read_usecase.dart';

class NotificationUsecases {
  final NotificationAddUsecase add;
  final NotificationGetListUsecase getList;
  final NotificationReadUsecase read;

  NotificationUsecases(NotificationRepository repository)
      : add = NotificationAddUsecase(repository),
        getList = NotificationGetListUsecase(repository),
        read = NotificationReadUsecase(repository);
}
