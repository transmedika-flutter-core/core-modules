import 'package:core_model/network/base/base_result.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:core_network/base/base_usecase_service.dart';

class NotificationGetListUsecase
    implements UseCaseService<List<NotificationEntity>, dynamic> {
  final NotificationRepository _repository;

  NotificationGetListUsecase(NotificationRepository repository): _repository = repository;

  @override
  Future<Result<List<NotificationEntity>>> call(dynamic param) {
    return _repository.getNotifications();
  }
}
