import 'package:core_model/network/base/base_result.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:core_network/base/base_usecase_service.dart';

class NotificationAddUsecase
    implements UseCaseService<int, NotificationEntity> {
  final NotificationRepository _repository;

  NotificationAddUsecase(NotificationRepository repository): _repository = repository;

  @override
  Future<Result<int>> call(NotificationEntity param) {
    return _repository.addNotification(param);
  }
}
