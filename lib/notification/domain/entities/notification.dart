// ignore_for_file: public_member_api_docs, sort_constructors_first
class NotificationEntity {
  final int? id;
  final String title;
  final String body;
  final int? code;
  final int time;
  final String? data;
  final String? image;
  final int read;

  NotificationEntity({
    this.id,
    required this.title,
    required this.body,
    required this.code,
    required this.time,
    required this.data,
    required this.image,
    this.read = 0,
  });

  @override
  String toString() {
    return 'NotificationEntity(id: $id, title: $title, body: $body, code: $code, time: $time, data: $data, image: $image, read: $read)';
  }
}
