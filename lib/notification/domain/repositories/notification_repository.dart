import 'package:core_model/network/base/base_result.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';

abstract class NotificationRepository {
  Future<Result<int>> addNotification(NotificationEntity param);
  Future<Result<List<NotificationEntity>>> getNotifications();
  Future<Result<int>> readNotifications(int id);
  Future<Result<int>> clearNotifications();
}
