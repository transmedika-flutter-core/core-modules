import 'package:core_modules/notification/data/datasource/notification_local_datasource.dart';
import 'package:core_modules/notification/data/repositories/notification_repository_impl.dart';
import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:core_modules/notification/domain/usecases/notification_add_usecase.dart';
import 'package:core_modules/notification/domain/usecases/notification_usecases.dart';
import 'package:core_modules/notification/presentation/bloc/notification_list_bloc.dart';
import 'package:get_it/get_it.dart';

class NotificationBaseInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependenciesMain(
      NotificationLocalDataSource notificationLocalDataSource) {
    if (!_getIt.isRegistered<NotificationRepository>()) {
      _getIt.registerSingleton<NotificationRepository>(
          NotificationRepositoryImpl(notificationLocalDataSource));
    }

    if (!_getIt.isRegistered<NotificationUsecases>()) {
      _getIt.registerFactory<NotificationUsecases>(
          () => NotificationUsecases(_getIt()));
    }
  }

  static void registerDependencies() {
    _getIt.registerFactory<NotificationListBloc>(() => NotificationListBloc(
      usecases: _getIt(),
    ));
  }

  static void unRegisterDependencies() {
    _getIt.unregister<NotificationListBloc>();
  }

  static NotificationListBloc getNotificationListBloc() {
    return _getIt.get<NotificationListBloc>();
  }

  static NotificationAddUsecase getNotificationAddUsecase() {
    return _getIt.get<NotificationUsecases>().add;
  }
}
