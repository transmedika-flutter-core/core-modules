import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin
      _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  static const String _defaultNotifChannel = "defaultChannel";

  static final AndroidNotificationChannel defaultMainChannel =
      AndroidNotificationChannel(
    _defaultNotifChannel,
    "Main notification channel",
    description:
        "This is for main notification channel for max important message",
    importance: Importance.max,
  );

  static void initialize({
    required AndroidNotificationChannel androidMainChannel,
    required String androidDefaultIcon,
    DidReceiveNotificationResponseCallback? onDidReceiveNotificationResponse,
  }) async {
    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings(
            androidDefaultIcon); //'@drawable/ic_notification'
    DarwinInitializationSettings darwinInitializationSettings =
        DarwinInitializationSettings();
    InitializationSettings initializationSettings = InitializationSettings(
      android: androidInitializationSettings,
      iOS: darwinInitializationSettings,
    );

    createChannel(androidMainChannel);
    await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse: onDidReceiveNotificationResponse);
  }

  static Future<void> showNotif(
    RemoteMessage message, {
    required AndroidNotificationChannel androidChannel,
    required String androidDefaultIcon,
    required Color androidDefaultIconColor, //const Color(0xFF009642)
  }) async {
    final notification = message.notification;
    if (notification == null) return;
    debugPrint("title: ${notification.title}");
    debugPrint("body: ${notification.body}");
    final imageSrc =
        notification.android?.imageUrl ?? notification.apple?.imageUrl;
    BigPictureStyleInformation? bigPictureStyleInformation;
    if (imageSrc is String) {
      final http.Response response = await http.get(Uri.parse(imageSrc));
      final image = response.bodyBytes;
      bigPictureStyleInformation = BigPictureStyleInformation(
        ByteArrayAndroidBitmap.fromBase64String(base64Encode(image)),
        largeIcon: ByteArrayAndroidBitmap.fromBase64String(base64Encode(image)),
      );
    }
    final androidPlatformChannel = AndroidNotificationDetails(
        androidChannel.id, androidChannel.name,
        importance: androidChannel.importance,
        priority: Priority.high,
        channelDescription: androidChannel.description,
        icon: notification.android?.smallIcon ??
            androidDefaultIcon, //"@drawable/ic_notification",
        color: androidDefaultIconColor,
        styleInformation: bigPictureStyleInformation);
    const iOSPlatformChannelSpecifics =
        DarwinNotificationDetails(presentSound: true);
    final notificationDetails = NotificationDetails(
      android: androidPlatformChannel,
      iOS: iOSPlatformChannelSpecifics,
    );
    _flutterLocalNotificationsPlugin.show(
      notification.hashCode,
      notification.title,
      notification.body,
      notificationDetails,
      payload: jsonEncode(message.data),
    );
  }

  static void createChannel(AndroidNotificationChannel channel) async {
    await _flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
  }
}
