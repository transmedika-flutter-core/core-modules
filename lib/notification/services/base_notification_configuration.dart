import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

abstract class BaseNotificationConfiguration {
  abstract final String mainTopic;
  abstract final AndroidNotificationChannel androidNotificationMainChannel;
  abstract final String androidNotificationDefaultIcon;
  abstract final Color androidNotificationDefaultIconColor;
}