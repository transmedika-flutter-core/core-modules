import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:core_modules/notification/domain/usecases/notification_usecases.dart';
import 'package:core_modules/notification/presentation/bloc/notification_list_event.dart';
import 'package:core_modules/notification/presentation/bloc/notification_list_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationListBloc
    extends MyBaseBloc<NotificationListEvent, NotificationListUiState> {
  final NotificationUsecases _usecases;

  NotificationListBloc({
    required NotificationUsecases usecases,
  })  : _usecases = usecases,
        super(NotificationListUiState()) {
    onRequestUsecase<OnNotificationGetListEvent, List<NotificationEntity>,
        dynamic>(usecases.getList);
    on<OnReadNotificationEvent>(_onReadNotifUsecase);
  }

  _onReadNotifUsecase(OnReadNotificationEvent event,
      Emitter<NotificationListUiState> emitter) async {
    final resultRead = await _usecases.read.call(event.id);
    if (resultRead is ResultSuccess) {
      final result = await _usecases.getList.call(Object());
      final currentState = state.requestState
          .copyWith(data: result.data, requestStatus: RequestStatus.success);
      emitter(state.copyWith(requestState: currentState));
    }
  }
}
