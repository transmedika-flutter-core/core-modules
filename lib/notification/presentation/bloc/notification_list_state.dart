import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';

class NotificationListState extends RequestState<List<NotificationEntity>> {
  NotificationListState(
      {this.data = const [],
      this.requestStatus = RequestStatus.idle,
      this.exception});

  @override
  final List<NotificationEntity> data;

  @override
  final Exception? exception;

  @override
  final RequestStatus requestStatus;

  @override
  NotificationListState copyWith(
      {List<NotificationEntity>? data,
      RequestStatus? requestStatus,
      Exception? exception}) {
    return NotificationListState(
        data: data ?? this.data,
        requestStatus: requestStatus ?? this.requestStatus,
        exception: exception ?? this.exception);
  }
}

class NotificationListUiState
    extends BaseUiState<List<NotificationEntity>, NotificationListState> {
  NotificationListUiState([NotificationListState? mRequestState])
      : requestState = mRequestState ?? NotificationListState();

  @override
  final NotificationListState requestState;

  @override
  NotificationListUiState copyWith({NotificationListState? requestState}) {
    return NotificationListUiState(requestState ?? this.requestState);
  }

  @override
  List<Object?> get props => [requestState];
}
