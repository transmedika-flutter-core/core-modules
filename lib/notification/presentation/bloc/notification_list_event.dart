import 'package:core_bloc/base_state.dart';

abstract class NotificationListEvent<T> extends BaseUiUIEvent<T> {}

class OnNotificationGetListEvent extends NotificationListEvent<dynamic>
    with UiEventParam<dynamic> {
  @override
  get param => Object();
}

class OnReadNotificationEvent extends NotificationListEvent<int> {
  final int id;
  OnReadNotificationEvent(this.id);
}
