import 'package:core_modules/notification/data/models/notification.dart';

abstract class NotificationLocalDataSource {
  Future<int> addNotification(NotificationDAO param);
  Future<List<NotificationDAO>> getNotifications();
  Future<int> readNotification(int id);
  Future<int> clearNotifications();
}