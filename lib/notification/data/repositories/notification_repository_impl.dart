import 'package:core_data/base/base_repository.dart';
import 'package:core_model/db/exception/db_execute_exception.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/exception/list_exception.dart';
import 'package:core_modules/notification/data/datasource/notification_local_datasource.dart';
import 'package:core_modules/notification/data/mappers/notification_dao_mapper.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:core_modules/notification/domain/repositories/notification_repository.dart';
import 'package:flutter/material.dart';

class NotificationRepositoryImpl extends BaseRepository
    implements NotificationRepository {
  final NotificationLocalDataSource _notificationDataSource;
  NotificationRepositoryImpl(NotificationLocalDataSource notificationDataSource)
      : _notificationDataSource = notificationDataSource;

  @override
  Future<Result<int>> addNotification(NotificationEntity param) async {
    final result = await _notificationDataSource.addNotification(param.toDAO());
    if (result != 0) {
      return ResultSuccess(data: result);
    } else {
      return ResultError(error: InsertTableDBException());
    }
  }

  @override
  Future<Result<List<NotificationEntity>>> getNotifications() async {
    final result = await _notificationDataSource.getNotifications();
    debugPrint("GET local Notifications: $result");
    if (result.isNotEmpty) {
      return ResultSuccess(data: result.map((e) => e.toEntity()).toList());
    } else {
      return ResultError(error: EmptyListException("empty notif"));
    }
  }

  @override
  Future<Result<int>> clearNotifications() async {
    final result = await _notificationDataSource.clearNotifications();
    if (result != 0) {
      return ResultSuccess(data: result);
    } else {
      return ResultError(error: DeleteTableDBException());
    }
  }

  @override
  Future<Result<int>> readNotifications(int id) async {
    final result = await _notificationDataSource.readNotification(id);
    if (result != 0) {
      return ResultSuccess(data: result);
    } else {
      return ResultError(error: UpdateTableDBException());
    }
  }
}
