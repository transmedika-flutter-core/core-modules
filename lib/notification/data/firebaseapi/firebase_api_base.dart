import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

abstract class FirebaseApiBase {
  final _firebaseMessaging = FirebaseMessaging.instance;

  Future<void> initNotification() async {
    NotificationSettings settings =
        await _firebaseMessaging.requestPermission();

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      debugPrint('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      debugPrint('User granted provisional permission');
    } else {
      debugPrint('User declined or has not accepted permission');
    }

    final fcmToken = await _firebaseMessaging.getToken();
    debugPrint("Token: $fcmToken");

    subscribeTopic(ccksUpdateTopic);

    onInit();
  }

  abstract final String ccksUpdateTopic;

  Future<void> onInit() async {}

  static Future<void> subscribeTopic(String topic) async {
    await FirebaseMessaging.instance.subscribeToTopic(topic);
  }

  static Future<String?> getFcmToken() {
    return FirebaseMessaging.instance.getToken();
  }

  static Future<void> setupOnOpenNotif(
      void Function(RemoteMessage? remoteMessage) onData) async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      onData(initialMessage);
    }

    FirebaseMessaging.onMessageOpenedApp.listen(onData);
  }

  static void setBackgroundNotif(BackgroundMessageHandler handler) {
    FirebaseMessaging.onBackgroundMessage(handler);
  }

  static void setForegroundNotif(void Function(RemoteMessage event)? onData,
      {Function? onError, void Function()? onDone, bool? cancelOnError}) {
    FirebaseMessaging.onMessage.listen(onData,
        onError: onError, onDone: onDone, cancelOnError: cancelOnError);
  }
}
