import 'package:core_modules/notification/data/mappers/payload_message_mapper.dart';
import 'package:core_modules/notification/data/notification_code_base.dart';
import 'package:core_modules/notification/data/notification_data.dart';
import 'package:core_modules/notification/data/notification_type.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';

extension ExNotificationEntity on NotificationEntity {
  NotificationData toNotificationData() {
    Map<String, dynamic>? mData;
    if (data != null) {
      mData = data!.fromDataToMap();
    }
    final NotificationType type;
    if (code == null) {
      type = NotificationType.info;
    } else if (code.toString() == NotificationCodeBase.updateApp.toString()) {
      type = NotificationType.updateVersion;
    } else {
      type = NotificationType.action;
    }
    return NotificationData(type, code: code, data: mData);
  }
}