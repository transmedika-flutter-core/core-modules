
import 'package:core_modules/notification/data/models/notification.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';

extension ExtNotificationDao on NotificationDAO {
  NotificationEntity toEntity() {
    return NotificationEntity(
      id: id,
      title: title,
      body: body,
      code: code,
      time: time,
      data: data,
      image: image,
      read: read,
    );
  }
}

extension ExtNotificationEntity on NotificationEntity {
  NotificationDAO toDAO() {
    return NotificationDAO(
      id: id,
      title: title,
      body: body,
      code: code,
      time: time,
      data: data,
      image: image,
      read: read,
    );
  }
}
