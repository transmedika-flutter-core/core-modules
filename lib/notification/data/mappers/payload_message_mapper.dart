import 'dart:convert';

import 'package:core_modules/notification/data/notification_code_base.dart';
import 'package:core_modules/notification/data/notification_data.dart';
import 'package:core_modules/notification/data/notification_type.dart';

extension ExtPayloadMapper on String {
  NotificationData? toNotificationData() {
    Map<String, dynamic>? notifData = jsonDecode(this);
    final code = notifData?['code'];
    Map<String, dynamic>? mData;
    if (notifData?['data'] is String) {
      final String? dataStr = notifData?['data'];
      if (dataStr != null) {
        mData = dataStr.fromDataToMap();
      }
    } else if (notifData?['data'] is Map<String, dynamic>) {
      mData = notifData?['data'];
    }
    final NotificationType type;
    if (code == null) {
      type = NotificationType.info;
    } else if (code.toString() == NotificationCodeBase.updateApp.toString()) {
      type = NotificationType.updateVersion;
    } else {
      type = NotificationType.action;
    }
    int? codeInt = code is int ? code : null;
    try {
      codeInt = code is String ? int.tryParse(code) : null;
    } catch (e) {
      codeInt = null;
    }
    return NotificationData(type, code: codeInt, data: mData);
  }

  Map<String, dynamic>? fromDataToMap() {
    final resultData = jsonDecode(this);
    if (resultData is Map<String, dynamic>?) {
      return resultData;
    }
    return null;
  }
}
