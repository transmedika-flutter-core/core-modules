import 'dart:convert';

import 'package:core_modules/notification/data/notification_code_base.dart';
import 'package:core_modules/notification/data/notification_data.dart';
import 'package:core_modules/notification/data/notification_type.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

extension ExtRemoteMessage on RemoteMessage? {
  NotificationData? toNotificationData() {
    if (this == null) return null;

    final code = this?.data['code'];
    Map<String, dynamic>? mData;
    if (this?.data['data'] is String) {
      final String? dataStr = this?.data['data'];
      if (dataStr != null) {
        final resultData = jsonDecode(dataStr);
        if (resultData is Map<String, dynamic>?) {
          mData = resultData;
        }
      }
    }
    final NotificationType type;
    if (code == null) {
      type = NotificationType.info;
    } else if (code.toString() == NotificationCodeBase.updateApp.toString()) {
      type = NotificationType.updateVersion;
    } else {
      type = NotificationType.action;
    }

    int? codeInt = code is int ? code : null;
    try {
      codeInt = code is String ? int.tryParse(code) : null;
    } catch (e) {
      codeInt = null;
    }
    return NotificationData(type, code: codeInt, data: mData);
  }

  NotificationEntity? toEntity() {
    if (this == null || this?.notification == null) return null;
    final notif = this!.notification!;
    Map<String, dynamic>? mData;
    if (this?.data['data'] is String) {
      final String? dataStr = this?.data['data'];
      if (dataStr != null) {
        final resultData = jsonDecode(dataStr);
        if (resultData is Map<String, dynamic>?) {
          mData = resultData;
        }
      }
    }

    final code = this?.data['code'];
    int? codeInt = code is int ? code : null;
    try {
      codeInt = code is String ? int.tryParse(code) : null;
    } catch (e) {
      codeInt = null;
    }

    
    final image = notif.android?.imageUrl ?? notif.apple?.imageUrl;
    return NotificationEntity(
      title: notif.title ?? "",
      body: notif.body ?? "",
      code: codeInt,
      time: DateTime.now().millisecondsSinceEpoch,
      data: mData != null ? jsonEncode(mData) : null,
      image: image,
    );
  }
}
