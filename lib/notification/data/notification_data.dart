import 'package:core_modules/notification/data/notification_type.dart';

class NotificationData {
  final NotificationType type;
  final int? code;
  final Map<String, dynamic>? data;

  NotificationData(this.type, {this.code, this.data});

  @override
  String toString() => 'NotificationData(type: $type, code: $code, data: $data)';
}
