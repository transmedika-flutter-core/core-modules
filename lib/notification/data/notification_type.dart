enum NotificationType {
  action, // jika notif punya code dan ada action ke suatu fitur
  info, // hanya sebatas info saja, jika diklik hanya akan membuka app / dismiss
  updateVersion, // jika ada update version
}