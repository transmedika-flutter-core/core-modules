import 'dart:convert';

class NotificationDAO {
  final int? id;
  final String title;
  final String body;
  final int? code;
  final int time;
  final String? data;
  final String? image;
  final int read;

  NotificationDAO({
    this.id,
    required this.title,
    required this.body,
    required this.code,
    required this.time,
    required this.data,
    required this.image,
    this.read = 0,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'title': title,
      'body': body,
      'code': code,
      'time': time,
      'data': data,
      'image': image,
      'read': read,
    };
  }

  factory NotificationDAO.fromMap(Map<String, dynamic> map) {
    return NotificationDAO(
      id: map['id'] != null ? map['id'] as int : null,
      title: map['title'] as String,
      body: map['body'] as String,
      code: map['code'] != null ? map['code'] as int : null,
      time: map['time'] as int,
      data: map['data'] != null ? map['data'] as String : null,
      image: map['image'] != null ? map['image'] as String : null,
      read: map['read'] as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory NotificationDAO.fromJson(String source) =>
      NotificationDAO.fromMap(json.decode(source) as Map<String, dynamic>);
}
