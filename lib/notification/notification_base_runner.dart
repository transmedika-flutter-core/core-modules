import 'package:core_modules/notification/data/firebaseapi/firebase_api_base.dart';
import 'package:core_modules/notification/data/mappers/payload_message_mapper.dart';
import 'package:core_modules/notification/data/mappers/remote_message_mapper.dart';
import 'package:core_modules/notification/data/notification_data.dart';
import 'package:core_modules/notification/domain/entities/notification.dart';
import 'package:core_modules/notification/services/base_notification_configuration.dart';
import 'package:core_modules/notification/services/local_notification_service.dart';
import 'package:flutter/foundation.dart';

import '../di/helpers/show_alice.dart';

class NotificationBaseRunner {
  static Future<void> runListener(
      {required BaseNotificationConfiguration config,
      required Function(NotificationData? data) onGetNotif,
      Function(NotificationEntity notificationEntity)?
          onForegroundNotif}) async {
    FirebaseApiBase.subscribeTopic(config.mainTopic);

    FirebaseApiBase.setupOnOpenNotif((remoteMessage) {
      onGetNotif.call(remoteMessage.toNotificationData());
    });

    LocalNotificationService.initialize(
        androidMainChannel: config.androidNotificationMainChannel,
        androidDefaultIcon: config.androidNotificationDefaultIcon,
        onDidReceiveNotificationResponse: (notifResponse) async {
          debugPrint("Notif Response: $notifResponse");
          if (notifResponse.payload != null) {
            if (kDebugMode &&
                notifResponse.id == 0 &&
                notifResponse.payload == '') {
              showInspectorAliceIfAvailable();
            } else {
              onGetNotif.call(notifResponse.payload!.toNotificationData());
            }
          }
        });

    FirebaseApiBase.setForegroundNotif((event) async {
      await LocalNotificationService.showNotif(
        event,
        androidChannel: config.androidNotificationMainChannel,
        androidDefaultIcon: config.androidNotificationDefaultIcon,
        androidDefaultIconColor: config.androidNotificationDefaultIconColor,
      );
      final param = event.toEntity();
      if (param != null) {
        onForegroundNotif?.call(param);
      }
    });
  }
}
