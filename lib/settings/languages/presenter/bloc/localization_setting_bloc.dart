import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_modules/settings/languages/domain/usecases/localization_usecases.dart';
import 'package:core_modules/settings/languages/presenter/bloc/localization_setting_event.dart';
import 'package:core_modules/settings/languages/presenter/bloc/localization_setting_state.dart';

class LocalizationSettingBloc
    extends MyBaseBloc<LocalizationSettingEvent, LocalizationSettingUIState> {
  LocalizationSettingBloc(LocalizationSettingUsecases localizationSettingUsecase)
      : super(LocalizationSettingUIState(
          requestState: LocalizationSettingState(),
        )) {
    onRequestUsecase<GetLocalizationChoosed, LocalizationSettingEntity, dynamic>(
      localizationSettingUsecase.get,
      onLoading: () async {
        return state.copyWith(
          requestState: state.requestState.copyWith(
            updateLocalizationStatus: RequestStatus.idle,
            requestStatus: RequestStatus.loading,
          ),
        );
      },
    );

    onRequestUsecase<SetLocalization, LocalizationSettingEntity, LocalizationSettingEntity>(
      localizationSettingUsecase.set,
      onLoading: () async {
        return state.copyWith(
            requestState: state.requestState.copyWith(
                updateLocalizationStatus: RequestStatus.loading,
                requestStatus: RequestStatus.loading));
      },
      onSuccessResult: (result) async {
        return state.copyWith(
          requestState: state.requestState.copyWith(
            data: result,
            updateLocalizationStatus: RequestStatus.success,
            requestStatus: RequestStatus.success,
          ),
        );
      },
      onErrorResult: (exception) async {
        var requestState = state.requestState;
        return state.copyWith(
          requestState: requestState.copyWith(
            exception: exception,
            updateLocalizationStatus: RequestStatus.failure,
            requestStatus: RequestStatus.failure,
          ),
        );
      },
    );
  }
}
