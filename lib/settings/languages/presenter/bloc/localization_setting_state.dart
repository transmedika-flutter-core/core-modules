import 'package:core_bloc/base_state.dart';
import 'package:core_bloc/request_status.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_constants/constants.dart';
import 'package:core_modules/settings/languages/domain/entities/localization_lang.dart';

class LocalizationSettingState extends RequestState<LocalizationSettingEntity> {
  @override
  final LocalizationSettingEntity? data;
  @override
  final Exception? exception;
  @override
  final RequestStatus requestStatus;

  final RequestStatus updateLocalizationStatus;

  LocalizationSettingState({
    LocalizationSettingEntity? data,
    this.requestStatus = RequestStatus.idle,
    this.updateLocalizationStatus = RequestStatus.idle,
    this.exception,
  }) : data = data ??
            LocalizationSettingEntity(Constants.defaultLocale);

  @override
  LocalizationSettingState copyWith({
    LocalizationSettingEntity? data,
    RequestStatus? requestStatus,
    RequestStatus? updateLocalizationStatus,
    Exception? exception,
  }) {
    return LocalizationSettingState(
      data: data ?? this.data,
      requestStatus: requestStatus ?? this.requestStatus,
      updateLocalizationStatus:
          updateLocalizationStatus ?? this.updateLocalizationStatus,
      exception: exception ?? this.exception,
    );
  }

  @override
  List<Object?> get props => super.props..add(updateLocalizationStatus);
}

class LocalizationSettingUIState
    extends BaseUiState<LocalizationSettingEntity, LocalizationSettingState> {
  final List<LocalizationLang> dataLocalization = LocalizationLang.data;

  @override
  final LocalizationSettingState requestState;

  LocalizationSettingUIState({required this.requestState});

  @override
  LocalizationSettingUIState copyWith(
      {LocalizationSettingState? requestState}) {
    return LocalizationSettingUIState(
        requestState: requestState ?? this.requestState);
  }

  @override
  List<Object?> get props => [requestState, dataLocalization];
}
