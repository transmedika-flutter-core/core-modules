import 'package:core_bloc/base_state.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_modules/settings/languages/domain/entities/localization_lang.dart';

abstract class LocalizationSettingEvent<PARAM> extends BaseUiUIEvent<PARAM> {}

class GetLocalizationChoosed extends LocalizationSettingEvent<dynamic>
    with UiEventParam<dynamic> {
  @override
  void get param => Object;
}

class SetLocalization
    extends LocalizationSettingEvent<LocalizationSettingEntity>
    with UiEventParam<LocalizationSettingEntity> {
  final LocalizationLang localizationLang;
  SetLocalization(this.localizationLang);

  @override
  LocalizationSettingEntity get param =>
      localizationLang.localizationSettingEntity;
}
