import 'package:core_modules/settings/languages/domain/usecases/localization_usecases.dart';
import 'package:core_modules/settings/languages/presenter/bloc/localization_setting_bloc.dart';
import 'package:get_it/get_it.dart';

class SettingLanguageInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies() {
    _regIfIsRegisteredFactory<LocalizationSettingUsecases>(
        () => LocalizationSettingUsecases(_getIt()));

    _regIfIsRegisteredFactory<LocalizationSettingBloc>(
        () => LocalizationSettingBloc(_getIt()));
  }

  static void unRegisterDependencies() {
    _unRegIfIsRegistered<LocalizationSettingUsecases>();
    _unRegIfIsRegistered<LocalizationSettingBloc>();
  }

  static void _regIfIsRegisteredFactory<T extends Object>(
    FactoryFunc<T> factoryFunc, {
    String? instanceName,
  }) {
    if (!_getIt.isRegistered<T>()) {
      _getIt.registerFactory<T>(factoryFunc, instanceName: instanceName);
    }
  }

  static void _unRegIfIsRegistered<T extends Object>() {
    if (_getIt.isRegistered<T>()) {
      _getIt.unregister<T>();
    }
  }

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }
}
