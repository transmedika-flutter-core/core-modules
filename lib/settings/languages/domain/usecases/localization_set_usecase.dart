import 'package:core_data/setting/setting_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_network/base/base_usecase_service.dart';

class LocalizationSetUsecase
    extends UseCaseService<LocalizationSettingEntity, LocalizationSettingEntity> {
  final SettingRepositoryService repositoryService;
  LocalizationSetUsecase(this.repositoryService);

  @override
  Future<Result<LocalizationSettingEntity>> call(LocalizationSettingEntity param) async {
    return repositoryService.setLocalizationSetting(param);
  }
}
