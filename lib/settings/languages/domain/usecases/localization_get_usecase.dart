import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:core_data/setting/setting_repository_service.dart';

class LocalizationGetUsecase
    extends UseCaseService<LocalizationSettingEntity, dynamic> {
  final SettingRepositoryService repositoryService;
  LocalizationGetUsecase(this.repositoryService);

  @override
  Future<Result<LocalizationSettingEntity>> call([dynamic param]) {
    return repositoryService.getLocalizationSetting();
  }
}
