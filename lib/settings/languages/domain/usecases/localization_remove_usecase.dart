import 'package:core_data/setting/setting_repository_service.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_network/base/base_usecase_service.dart';

class LocalizationRemoveUsecase extends UseCaseService<dynamic, void> {
  final SettingRepositoryService repositoryService;
  LocalizationRemoveUsecase(this.repositoryService);

  @override
  Future<Result> call([void param]) {
    return repositoryService.removeLocalizationSetting();
  }
}
