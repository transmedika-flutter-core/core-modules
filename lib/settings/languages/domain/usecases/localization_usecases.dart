import 'package:core_data/setting/setting_repository_service.dart';
import 'localization_get_usecase.dart';
import 'localization_remove_usecase.dart';
import 'localization_set_usecase.dart';

class LocalizationSettingUsecases {
  final LocalizationGetUsecase get;
  final LocalizationRemoveUsecase remove;
  final LocalizationSetUsecase set;
  
  LocalizationSettingUsecases(SettingRepositoryService repositoryService)
      : get = LocalizationGetUsecase(repositoryService),
        remove = LocalizationRemoveUsecase(repositoryService),
        set = LocalizationSetUsecase(repositoryService);
}
