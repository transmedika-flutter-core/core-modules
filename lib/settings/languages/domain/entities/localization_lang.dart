import 'package:core_constants/constants.dart';
import 'package:core_model/ui/localization_setting_entity.dart';
import 'package:flutter/material.dart';

class LocalizationLang {
  final String name;
  final LocalizationSettingEntity localizationSettingEntity;

  LocalizationLang({
    required this.name,
    required this.localizationSettingEntity,
  });

  bool hasChoose(LocalizationSettingEntity? localizationSettingEntity) {
    return this.localizationSettingEntity == localizationSettingEntity;
  }

  static List<LocalizationLang> get data => [
        LocalizationLang(
            name: "🇮🇩    Indonesia",
            localizationSettingEntity:
                LocalizationSettingEntity(Constants.localeID)),
        LocalizationLang(
            name: "🇬🇧    English",
            localizationSettingEntity:
                LocalizationSettingEntity(Constants.localeEN)),
      ];

  static List<Locale> get supportedLocale => [
        const Locale(Constants.localeID),
        const Locale(Constants.localeEN),
      ];
}
